<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegionTitleField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function ($table) {
            $table->string('title_en')->after('lng');
            $table->string('title_ru')->after('lng');
            $table->string('title_uk')->after('lng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regions', function ($table) {
            $table->dropColumn('title_en');
            $table->dropColumn('title_ru');
            $table->dropColumn('title_uk');
        });
    }
}
