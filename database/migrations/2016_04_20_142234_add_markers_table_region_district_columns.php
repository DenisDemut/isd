<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarkersTableRegionDistrictColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('markers', function ($table) {
             $table->integer('region')->unsigned();
             $table->integer('district')->unsigned();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('markers', function ($table) {
             $table->dropColumn('region');
             $table->dropColumn('district');
         });
     }
}
