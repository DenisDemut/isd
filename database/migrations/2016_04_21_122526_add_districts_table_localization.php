<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistrictsTableLocalization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('districts', function ($table) {
            $table->renameColumn('name', 'name_uk');
            $table->string('name_ru')->after('name');
            $table->string('name_en')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('districts', function ($table) {
            $table->renameColumn('name_uk', 'name');
            $table->dropColumn('name_ru');
            $table->dropColumn('name_en');
        });
    }
}
