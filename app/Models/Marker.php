<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marker extends Model
{
    protected $fillable = ['id', 'name_uk', 'name_ru', 'name_en', 'region', 'district', 'g_type', 'lat', 'lng'];
    protected $hidden = ['created_at', 'updated_at'];
}
