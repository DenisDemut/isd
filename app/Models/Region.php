<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = ['name_uk', 'name_ru', 'name_en', 'title_uk', 'title_ru', 'title_en', 'lat', 'lng', 'g_type'];
    protected $hidden = ['created_at', 'updated_at'];

    public function districts()
    {
        return $this->hasMany('App\Models\District', 'region');
    }
    public function markers()
    {
        return $this->hasMany('App\Models\Marker', 'region')->where('district', 0);
    }
    public function markersAll()
    {
        return $this->hasMany('App\Models\Marker', 'region');
    }
}
