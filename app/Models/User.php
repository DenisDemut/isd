<?php

namespace App\Models;

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Bican\Roles\Models\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use HasRoleAndPermission;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function photo()
    {
        return $this->morphOne('App\Models\Photo', 'owner')->where('type', '=', 'photo');
    }

    public function delete()
    {
        $this->photo()->delete();
        return parent::delete();
    }

    public static function createNew($data)
    {
        $user = new User();
        $user->name = array_get($data, 'name');
        $user->email = array_get($data, 'email');
        $user->password = Hash::make(array_get($data, 'password'));
        $user->save();
        $user->attachRole(array_get($data, 'role'));
        return $user;
    }

    public static function updateUser($data, $id)
    {
        $user = User::find($id);
        $roleId = array_get($data, 'role');
        $role = Role::find($roleId);
        if(!$user->is($role->slug)) {
            $user->detachRole($role);
            $user->attachRole($role);
        }
        return $user->update($data);
    }

    public static function deleteUser($id)
    {
        return User::find($id)->delete();
    }
    public static function updatePassword($data, $id)
    {
        return User::find($id)->update(['password' => Hash::make(array_get($data, 'password'))]);
    }

}
