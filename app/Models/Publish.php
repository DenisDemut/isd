<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Publish extends Model
{
    protected $fillable = ['name', 'g_type', 'current'];
    protected $hidden = ['updated_at'];

    public function getDateAttribute()
    {
        return $this->created_at->toDateString();
    }
    public function getTimeAttribute()
    {
        return $this->created_at->toTimeString();
    }
    public function delete()
    {
        $path = 'public/' . $this->g_type . '/' . $this->date . '/' . $this->time;
        if(Storage::exists($path)) {
            Storage::deleteDirectory($path);
        }
        return parent::delete();
    }
}
