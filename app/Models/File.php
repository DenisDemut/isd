<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class File extends Model
{
    protected $fillable = ['coordinates'];
    protected $hidden = ['owner_id', 'owner_type', 'created_at', 'updated_at'];


    public function getCoordinatesAttribute($value)
    {
        return json_decode($value);
    }

    public function owner()
    {
        return $this->morphTo();
    }

    public static function getWKTCoords($children, &$coordinates)
    {
        if($children->getName() == 'coordinates') {
            $coords = $children->asXML();
            $coords = trim(strip_tags($coords));
            $coords = explode(' ', $coords);
            $buf = [];
            for($i = 0; $i < count($coords); $i++) {
                $p = explode(',', $coords[$i]);
                $buf[] = $p[0] . ' ' . $p[1];
            }
            $coordinates[] = 'POLYGON((' . join(',', $buf) . '))';
        } else {
            foreach($children->children() as $key => $child) {
                self::getWKTCoords($child, $coordinates);
            }
        }
        return;
    }


    public static function saveFile($file)
    {
        $done = false;
        $fileContent = '';
        if(in_array($file->getClientOriginalExtension(), ['zip', 'kmz'])) {
            $zip = new \ZipArchive;
            $res = $zip->open($file->getPathName());
            if($res === true) {
                for($i = 0; $i < $zip->numFiles; $i++) {
                    $zipFileName = $zip->getNameIndex($i);
                    if(preg_match('/\.kml/', $zipFileName)) {
                        $zip->close();
                        $done = true;
                        $fileContent = file_get_contents('zip://' . $file->getPathName() . '#' . $zipFileName);
                        break;
                    }
                }
            }
        } else {
            $fileContent = file_get_contents($file->getPathName());
            $done = true;
        }
        if($done) {
            $xml = simplexml_load_string(utf8_encode($fileContent));
            $coordinates = [];
            foreach($xml->children() as $child) {
                self::getWKTCoords($child, $coordinates);
            }
            $f = File::create([
                'coordinates' => json_encode($coordinates)
            ]);

        } else {
            throw new \Exception('File upload failed');
        }

        return $f;
    }
}
