<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name', 'g_type'];
    protected $hidden = ['created_at', 'updated_at'];


    public function delete()
    {
        $this->file->delete();
        return parent::delete();
    }

    public function file()
    {
        return $this->morphOne('App\Models\File', 'owner');
    }


}
