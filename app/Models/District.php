<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = ['name_uk', 'name_ru', 'name_en', 'region', 'g_type'];
    protected $hidden = ['created_at', 'updated_at'];

    public function markers()
    {
        return $this->hasMany('App\Models\Marker', 'district')->where('region', $this->region);
    }
}
