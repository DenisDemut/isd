<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewCityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'uploaded_file' => 'required|mimes:kml,xml,kmz,zip'
        ];
    }
}
