<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Models\User;
use Auth;
use Session;


class AuthController extends Controller
{
    public function getAuth() {
		return view('app.auth');
	}
    public function getLogout() {
        Auth::logout();
        return redirect()->route('login');
    }
    public function postAuth(UserLoginRequest $request) {
		if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('remember') ? true : false)) {
            return redirect()->route('app');
		} else {
			return redirect()->back()->withErrors(['message' => 'Invalid e-mail and/or password']);
		}
	}
}
