<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\DistrictRequest;
use App\Models\District;

class DistrictController extends Controller
{
    public function getDistricts()
    {
        return response()->json(District::orderBy('name_ru', 'ASC')->get());
    }
    public function postDistrict(DistrictRequest $request)
    {
        $region = District::create($request->all());
        return response()->json($region);
    }
    public function putDistrict(DistrictRequest $request, $id)
    {
        $region = District::find($id)->update($request->all());
        return response()->json($region);
    }
    public function deleteDistrict(Request $request, $id)
    {
        District::find($id)->delete();
        return response()->json([]);
    }
}
