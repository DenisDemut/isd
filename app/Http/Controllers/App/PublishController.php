<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\City;
use App\Models\Marker;
use App\Models\Publish;
use App\Models\Region;
use Storage;
use KML;

class PublishController extends Controller
{
    public function getPublishes()
    {
        return response()->json(Publish::all());
    }
    public function getPublish($id)
    {
        Publish::find($id)->delete();
        return response()->json([]);
    }
    public function deletePublish(Request $request, $id)
    {
        Publish::find($id)->delete();
        return response()->json([]);
    }
    public function putPublish(Request $request, $id)
    {
        Publish::find($id)->update($request->all());
        return response()->json([]);
    }
    private function fromPublish($type, $path)
    {
        $publish = Publish::where('current', true)->first();
        $path = 'public/' . $type . '/' . $publish->date . '/' . $publish->time . '/' . $path;
        if(Storage::has($path)) {
            $file_content = Storage::get($path);
            return $file_content;
        } else {
            abort(404);
        }
    }

    public function getTempKMZ(Request $request, $type)
    {
        $path = 'public/temp/' . $type . '/map.kmz';
        if(Storage::has($path)) {
            return response()->download(storage_path('app') . "/" . $path);
        } else {
            abort(404);
        }
    }
    public function getTempKML(Request $request, $type)
    {
        $path = 'public/temp/' . $type . '/map.kml';
        if(Storage::has($path)) {
            return response()->download(storage_path('app') . "/" . $path);
        } else {
            abort(404);
        }
    }
    public function getKMZData(Request $request, $type)
    {
        return response($this->fromPublish($type, 'map.kmz'))
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-Disposition', 'attachment; filename="map.kmz');
    }
    public function getJSONData(Request $request, $type, $lang, $filename)
    {
        return response($this->fromPublish($type, $lang . '/' . $filename))->header('Content-Type', 'application/json');
    }

    public function postPublish(Request $request)
    {
        $type = $request->input('g_type', '3g');
        $publish = Publish::create([
            'g_type' => $type
        ]);
        $path = 'public/'. $type . '/' . $publish->date . '/' . $publish->time;
        if(!Storage::exists($path)) {
            Storage::makeDirectory($path);
        }
        if(!Storage::exists('public/' . $type . '/map.kml')) $xml = KML::buildKMZ($type);
        else $xml = Storage::get('public/' . $type . '/map.kml');
        $zip = new \ZipArchive();
        $zip->open(storage_path('app') . '/' . $path . '/map.kmz', \ZIPARCHIVE::CREATE);
        $zip->addFromString('doc.kml', $xml);
        $zip->close();

        $langs = ['ru', 'uk', 'en'];
        foreach($langs as $lang) {
            $regions = Region::where('g_type', $type)->get();
            Storage::put($path . '/' . $lang . '/points.json', json_encode(json_decode(view('json.region', ['regions' => $regions, 'lang' => $lang, 'type' => $type])), JSON_PRETTY_PRINT));
            Storage::put($path . '/' . $lang . '/points-by-regions.json', json_encode(json_decode(view('json.district_region', ['regions' => $regions, 'lang' => $lang, 'type' => $type])), JSON_PRETTY_PRINT));
            $regions = $regions->sortBy('name_' . $lang)->values()->all();
            Storage::put($path . '/' . $lang . '/states.json', json_encode(json_decode(view('json.states', ['regions' => $regions, 'lang' => $lang, 'type' => $type])), JSON_PRETTY_PRINT));
            Storage::put($path . '/' . $lang . '/coverages.json', json_encode(json_decode(view('json.coverage', ['lang' => $lang, 'type' => $type])), JSON_PRETTY_PRINT));
        }

        return view('app.publish_success', [
            'langs' => $langs,
            'type' => $type,
            'cities' => City::where('g_type', $type)->count(),
            'markers' => Marker::count()
        ]);


    }
}
