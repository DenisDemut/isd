<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\RegionRequest;
use App\Models\Region;

class RegionController extends Controller
{
    public function getRegions()
    {
        return response()->json(Region::orderBy('name_ru', 'ASC')->get());
    }
    public function postRegion(RegionRequest $request)
    {
        $region = Region::create($request->all());
        return response()->json($region);
    }
    public function putRegion(RegionRequest $request, $id)
    {
        $region = Region::find($id)->update($request->all());
        return response()->json($region);
    }
    public function deleteRegion(Request $request, $id)
    {
        Region::find($id)->delete();
        return response()->json([]);
    }
}
