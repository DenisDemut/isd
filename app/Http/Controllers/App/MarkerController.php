<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\MarkerRequest;
use App\Models\Marker;

class MarkerController extends Controller
{
    public function getMarkers()
    {
        return response()->json(Marker::orderBy('name_ru', 'ASC')->get());
    }
    public function postMarker(MarkerRequest $request)
    {
        $marker = Marker::create($request->all());
        return response()->json($marker);
    }
    public function putMarker(MarkerRequest $request, $id)
    {
        $marker = Marker::find($id);
        if($marker) {
            $marker->update($request->all());
            return response()->json($marker);
        } else {
            return $this->postMarker($request);
        }

    }
    public function deleteMarker(Request $request, $id)
    {
        Marker::find($id)->delete();
        return response()->json([]);
    }
}
