<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CityRequest;
use App\Http\Requests\NewCityRequest;
use App\Models\City;
use App\Models\File;
use Storage;
use KML;

class CityController extends Controller
{
    public function getCities()
    {
        $cities = City::with('file')->orderBy('name', 'ASC')->get();
        return response()->json($cities);
    }
    public function postCity(NewCityRequest $request)
    {
        $city = City::create($request->only(['name', 'g_type']));
        $file = File::saveFile($request->file('uploaded_file'));
        $city->file()->save($file);
        KML::buildKMZ($city->g_type);
        $city = City::with('file')->find($city->id);
        return response()->json($city);
    }
    public function putCity(CityRequest $request, $id)
    {
        $city = City::find($id);
        $city->update($request->only('name'));
        if($request->hasFile('uploaded_file')) {
            $file = File::saveFile($request->file('uploaded_file'));
            $city->file->delete();
            $city->file()->save($file);
        } else {
            $file = $request->input('file');
            $city->file()->update(['coordinates' => json_encode($file['coordinates'])]);
        }
        KML::buildKMZ($city->g_type);

        return response()->json(City::with('file')->find($city->id));
    }
    public function deleteCity(Request $request, $id)
    {
        $city = City::with('file')->find($id);
        $city->delete();
        KML::buildKMZ($city->g_type);
        return response()->json($city);
    }
    public function getCityKML(Request $request, $id)
    {
        $city = City::with('file')->find($id);
        $xml = KML::buildCityKML($city);
        return response($xml)
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-disposition', ' attachment; filename=' . $city->name . '.kml');
    }
}
