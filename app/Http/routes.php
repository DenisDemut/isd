<?php
Route::group(['middleware' => ['web']], function () {
    Route::get('auth', ['as' => 'login', 'uses' => 'AuthController@getAuth']);
    Route::post('auth', ['as' => 'login', 'uses' => 'AuthController@postAuth']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);

});

Route::group(['middleware' => ['web', 'role:admin']], function () {
    Route::get('/', ['as' => 'app', 'uses' => 'AppController@getApp']);
});

// Route::get('{publish}/file/{path}', function($path) {
//     return response()->download(storage_path('app') . "/" . $path);
// })->where('path', '.*')->where('publish', '[0-9]+');

Route::get('temp/{type}/map.kml', 'App\PublishController@getTempKML');
Route::get('{type}/map.kmz', 'App\PublishController@getKMZData');
Route::get('{type}/{lang}/{filename}', 'App\PublishController@getJSONData')->where('filename', '(points\-by\-regions\.json|points\.json|states\.json|coverages.json)');

Route::group(['prefix' => 'api/v1', 'middleware' => ['web']], function () {
    Route::get('cities', 'App\CityController@getCities');
    Route::post('cities', 'App\CityController@postCity');
    Route::match(['post', 'put'], 'cities/{id}', 'App\CityController@putCity');
    Route::get('cities/{id}/download', 'App\CityController@getCityKML');
    Route::delete('cities/{id}', 'App\CityController@deleteCity');


    Route::get('markers', 'App\MarkerController@getMarkers');
    Route::post('markers', 'App\MarkerController@postMarker');
    Route::put('markers/{id}', 'App\MarkerController@putMarker');
    Route::delete('markers/{id}', 'App\MarkerController@deleteMarker');

    Route::get('regions', 'App\RegionController@getRegions');
    Route::post('regions', 'App\RegionController@postRegion');
    Route::put('regions/{id}', 'App\RegionController@putRegion');
    Route::delete('regions/{id}', 'App\RegionController@deleteRegion');

    Route::get('districts', 'App\DistrictController@getDistricts');
    Route::post('districts', 'App\DistrictController@postDistrict');
    Route::put('districts/{id}', 'App\DistrictController@putDistrict');
    Route::delete('districts/{id}', 'App\DistrictController@deleteDistrict');

    Route::get('publishes', 'App\PublishController@getPublishes');
    Route::put('publishes/{id}', 'App\PublishController@putPublish');
    Route::post('publishes', 'App\PublishController@postPublish');
    Route::delete('publishes/{id}', 'App\PublishController@deletePublish');

});
