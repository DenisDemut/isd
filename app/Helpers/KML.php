<?php

use App\Models\City;
use App\Models\Marker;
use App\Models\Region;
use Storage;

class KML
{
    public static function getCityCoords($city)
    {
        $coords = $city->file->coordinates;
        $cityCoordinates = [];
        foreach($coords as $coordinate) {
            $normalized = str_replace(["POLYGON((", "))", ",", " "], ["", "", ",0 ", ","], $coordinate);
            $normalized = str_replace(",0,", ",0 ", $normalized);
            $cityCoordinates[] = $normalized . ',0';
        }
        return $cityCoordinates;

    }
    public static function getXML($gType, $coordinates, $name)
    {
        $types = json_decode(file_get_contents(public_path() . '/types.json'));
        $types = collect($types);
        $type = $types->where('key', $gType)->first();

        $xml = simplexml_load_string(view('kml.index', [
            'type' => $type,
            'coordinates' => $coordinates,
            'name' => $name
        ]));
        $domxml = new \DOMDocument('1.0');
        $domxml->preserveWhiteSpace = false;
        $domxml->formatOutput = true;
        $domxml->loadXML($xml->asXML());
        return $domxml->saveXML();
    }
    public static function buildKMZ($gType, $path = null)
    {


        $cities = City::where('g_type', $gType)->with('file')->get();
        if(!$path)
            $path = 'public/temp/' . $gType;

        $coordinates = [];
        foreach($cities as $city) {
            $cityCoordinates = self::getCityCoords($city);
            $coordinates[$city->id] = [
                'name' => $city->name,
                'coordinates' => $cityCoordinates
            ];
        }
        $xml = self::getXML($gType, $coordinates, 'Ukraine_coverage_' . date('Y_m_d'));

        if(!Storage::exists($path)) {
            Storage::makeDirectory($path);
        }
        file_put_contents(storage_path('app') . '/' . $path . '/map.kml', $xml);
        return $xml;

    }
    public static function buildCityKML($city)
    {
        $cityCoordinates = self::getCityCoords($city);
        $coordinates[$city->id] = [
            'name' => $city->name,
            'coordinates' => $cityCoordinates
        ];
        return self::getXML($city->g_type, $coordinates, $city->name);
    }
}
