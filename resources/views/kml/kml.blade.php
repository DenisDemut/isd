{!! '<'.'?xml version="1.0" encoding="UTF-8" ?'.'>' !!}
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
    <Document>
    	{!! $name !!}
        <styleUrl>#style_{{ $type }}</styleUrl>
        @if($type == '3g')
            <Style id="style_3g_1">
                <LineStyle>
                    <color>cc5b68fd</color>
                </LineStyle>
                <PolyStyle>
                    <color>985b68fd</color>
                </PolyStyle>
            </Style>
            <Style id="style_3g_2">
                <LineStyle>
                    <color>cc5b68fd</color>
                </LineStyle>
                <PolyStyle>
                    <color>985b68fd</color>
                </PolyStyle>
            </Style>
            <StyleMap id="style_3g">
                <Pair>
                    <key>normal</key>
                    <styleUrl>#style_3g_2</styleUrl>
                </Pair>
                <Pair>
                    <key>highlight</key>
                    <styleUrl>#style_3g_1</styleUrl>
                </Pair>
            </StyleMap>
        @elseif($type == '4g')
            <Style id="style_4g_1">
                <LineStyle>
                    <color>3d3d3dff</color>
                </LineStyle>
                <PolyStyle>
                    <color>4f4f4fff</color>
                </PolyStyle>
            </Style>
            <Style id="style_4g_2">
                <LineStyle>
                    <color>3d3d3dff</color>
                </LineStyle>
                <PolyStyle>
                    <color>4f4f4fff</color>
                </PolyStyle>
            </Style>
            <StyleMap id="style_4g">
                <Pair>
                    <key>normal</key>
                    <styleUrl>#style_4g_2</styleUrl>
                </Pair>
                <Pair>
                    <key>highlight</key>
                    <styleUrl>#style_4g_1</styleUrl>
                </Pair>
            </StyleMap>
        @endif
        <Folder>
            @foreach($coordinates as $i => $coordinate)
                <Placemark>
    				<name>{{ $i }}</name>
    				<styleUrl>#style_{{ $type }}</styleUrl>
    				<Polygon>
    					<tessellate>1</tessellate>
    					<outerBoundaryIs>
    						<LinearRing>
    							{!! $coordinate !!}
    						</LinearRing>
    					</outerBoundaryIs>
    				</Polygon>
    			</Placemark>
            @endforeach
        </Folder>
    </Document>
</kml>
