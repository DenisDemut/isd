{!! '<'.'?xml version="1.0" encoding="UTF-8" ?'.'>' !!}
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
    <Document>
    	<name>{{ $name }}</name>

        <Style id="style_1">
            <LineStyle>
                <color>{{ $type->style->line->ahex }}</color>
            </LineStyle>
            <PolyStyle>
                <color>{{ $type->style->poly->ahex }}</color>
            </PolyStyle>
        </Style>
        <Style id="style_2">
            <LineStyle>
                <color>{{ $type->style->line->ahex }}</color>
            </LineStyle>
            <PolyStyle>
                <color>{{ $type->style->poly->ahex }}</color>
            </PolyStyle>
        </Style>
        <StyleMap id="style_map">
            <Pair>
                <key>normal</key>
                <styleUrl>#style_2</styleUrl>
            </Pair>
            <Pair>
                <key>highlight</key>
                <styleUrl>#style_1</styleUrl>
            </Pair>
        </StyleMap>
        @foreach($coordinates as $coordinate)
            @foreach(array_get($coordinate, 'coordinates') as $i => $coords)
                <Placemark>
                    <name>{{ array_get($coordinate, 'name') }}_{{ $i }}</name>
                    <styleUrl>#style_map</styleUrl>
                        <Polygon>
                            <tessellate>1</tessellate>
                            <outerBoundaryIs>
                                <LinearRing>
                                    <coordinates>{!! $coords !!}</coordinates>
                                </LinearRing>
                            </outerBoundaryIs>
                        </Polygon>
                </Placemark>
            @endforeach
        @endforeach
    </Document>
</kml>
