<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Network Coverage Editor</title>
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK008Ks-0BOci-aqDhLuSoaHulz0n7AOw"></script>
    <script src="/js/app.js"></script>
</head>
<body class="hold-transition skin-black">
    <div class="wrapper">

        <header class="main-header">
            <a href="{{route('app')}}" class="logo">
                <span class="logo-mini"><b>QM</b></span>
                <span class="logo-lg">Network Coverage Editor</span>
            </a>
            @include('app.blocks.header')
        </header>
        <div class="content-wrapper">
            <section class="content">
                @yield('content')
            </section>
        </div>
        @include('app.blocks.error_reporting')
        @include('app.blocks.footer')
    </div>

</body>
</html>
