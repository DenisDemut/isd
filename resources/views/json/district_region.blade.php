{
    @foreach($regions as $rk => $region)
        "{{ $region->id }}": {
            "groups": [
                @if($region->markers->count() > 0)
                {
                    "items": [
                        @foreach($region->markers as $k => $marker)
                            {
                                "id": "{{ $marker->id }}",
                                "lat": "{{ $marker->lat }}",
                                "lon": "{{ $marker->lng }}",
                                "title": "{{ $marker->{'name_' . $lang} }}",
                                "description": "{{ $marker->{'name_' . $lang} }}",
                                "image": "http://nce.vodafone.ua/{{ $type }}/pointer.png"
                            }
                            @if(($k + 1) !== $region->markers->count())
                                ,
                            @endif
                        @endforeach
                    ]
                }
                @if($region->districts->count() > 0)
                    ,
                    @foreach($region->districts as $i => $district)
                        {
                            "items": [
                                @foreach($district->markers as $k => $marker)
                                    {
                                        "id": "{{ $marker->id }}",
                                        "lat": "{{ $marker->lat }}",
                                        "lon": "{{ $marker->lng }}",
                                        "title": "{{ $marker->{'name_' . $lang} }}",
                                        "description": "{{ $marker->{'name_' . $lang} }}",
                                        "image": "http://nce.vodafone.ua/{{ $type }}/pointer.png"
                                    }
                                    @if(($k + 1) !== $district->markers->count())
                                        ,
                                    @endif
                                @endforeach
                            ],
                            "title": "{{ $district->{'name_' . $lang} }}"
                        }
                        @if(($i + 1) !== $region->districts->count())
                            ,
                        @endif
                    @endforeach
                @endif
                @endif
            ],
            "region_title": "{{ $region->{'name_' . $lang} }}"
        }
        @if(($rk + 1) !== $regions->count())
            ,
        @endif
    @endforeach
}
