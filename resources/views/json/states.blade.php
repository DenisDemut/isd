[
    @foreach($regions as $rk => $region)
        {
            "id": "{{ $region->id }}",
            "title": "{{ $region->{'name_' . $lang} }}",
            "t_title": "{{ $region->{'name_' . $lang} }}",
            "t_title2": "{{ $region->{'title_' . $lang} }}",
            "lat": "{{ $region->lat }}",
            "lng": "{{ $region->lng }}"
        }
        @if(($rk + 1) !== count($regions))
            ,
        @endif
    @endforeach
]
