{
    "coverages": [
        {
            @if($lang == 'en')
            "name": "{{ strtoupper($type) }} coverage",
            @elseif($lang == 'ru')
            "name": "{{ strtoupper($type) }} покрытие",
            @elseif($lang == 'uk')
            "name": "{{ strtoupper($type) }} покриття",
            @endif
            "coverage_path": "https://www.vodafone.ua/{{ $type }}/map.kmz?{{ time() }}",
            "description": ""
        }
    ]
}
