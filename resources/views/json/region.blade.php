{
    @foreach($regions as $rk => $region)
        @if($region->markers->count() > 0)
            "{{ $region->id }}": {
                "items": {
                    @foreach($region->markers as $k => $marker)
                        "{{ $marker->id }}": {
                            "id": "{{ $marker->id }}",
                            "lat": "{{ $marker->lat }}",
                            "lon": "{{ $marker->lng }}",
                            "title": "{{ $marker->{'name_' . $lang} }}",
                            "image": "http://nce.vodafone.ua/{{ $type }}/pointer.png"
                        }
                        @if(($k + 1) !== $region->markers->count())
                            ,
                        @endif
                    @endforeach
                },
                "region_title": "{{ $region->{'name_' . $lang} }}"
            }

            @if(($rk + 1) !== $regions->count())
                ,
            @endif
        @endif
    @endforeach
}
