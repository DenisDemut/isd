<nav class="navbar navbar-static-top" role="navigation">
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @if(Auth::user()->photo)
                        <img src="/images/small/{{ Auth::user()->photo->path }}/{{ Auth::user()->photo->name }}" class="user-image" alt="User Image">
                    @endif
                    <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="user-header">
                        @if(Auth::user()->photo)
                            <img src="/images/small/{{ Auth::user()->photo->path }}/{{ Auth::user()->photo->name }}" class="img-circle" alt="User Image">
                        @endif
                        <p>
                            {{ Auth::user()->name }} | {{ Auth::user()->email }}
                            <small>Member since {{ Auth::user()->created_at->format('M. Y') }}</small>
                        </p>
                    </li>
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{-- route('admin-profile', Auth::user()->id) --}}" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Log out</a>
                        </div>
                    </li>
                </ul>
            </li>

        </ul>
    </div>

</nav>
