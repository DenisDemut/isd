@if(count($errors) > 0)
    <script>
        var notificationErrors = {!! json_encode($errors->all()) !!}
    </script>
@endif
@if(Session::has('warnings'))
    <script>
        var notificationWarnings = {!! json_encode(Session::pull('warnings')) !!}
    </script>

@endif
@if(Session::has('informations'))
    <script>
        var notificationInformations = {!! json_encode(Session::pull('information')) !!}
    </script>
@endif
@if(Session::has('successes'))
    <script>
        var notificationSuccesses = {!! json_encode(Session::pull('successes')) !!}
    </script>
@endif
