@extends('index')

@section('content')
    <div class="container-fluid">
        <div class="col-md-8">
            <div class="row menu-row">
                <div class="col-md-12">
                    <!-- Single button -->
                    <div class="btn-group types-list">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class='current-type'></span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu"></ul>
                    </div>
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-primary new-city">Add city</button>
                        <button type="button" class="btn btn-primary new-marker">Add marker</button>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li class='new-region'><a href="#">Add region</a></li>
                                <li class='new-district'><a href="#">Add district</a></li>
                            </ul>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary pull-right publish" data-loading-text="Loading...">Publish</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 sidebar">
            <div class='row menu-row'>
                <div class="col-md-12">
                    <div role="group" class="btn-group search-bar">

                    </div>

                </div>
            </div>
            <div class='row'>
                <div class="col-md-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#cities" aria-controls="cities" role="tab" data-toggle="tab">Cities</a></li>
                        <li role="presentation"><a href="#markers" aria-controls="markers" role="tab" data-toggle="tab">Markers</a></li>
                        <li role="presentation"><a href="#regions" aria-controls="regions" role="tab" data-toggle="tab">Regions</a></li>
                        <li role="presentation"><a href="#districts" aria-controls="districts" role="tab" data-toggle="tab">Districts</a></li>
                        <li role="presentation"><a href="#publishes" aria-controls="publishes" role="tab" data-toggle="tab">Publishes</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="cities">
                            <div class="scroll">
                                <div class="list-group cities-list"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="markers">
                            <div class="scroll">
                                <div class="list-group markers-list"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="regions">
                            <div class="scroll">
                                <div class="list-group regions-list"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="districts">
                            <div class="scroll">
                                <div class="list-group districts-list"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="publishes">
                            <div class="scroll">
                                <div class="list-group publishes-list"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
