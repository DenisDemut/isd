'use strict';

import 'bootstrap/js/modal';

import {
    View
} from 'backbone';

export default class Modal extends View {
    get events() {
        return {
            'click .submit': 'submit',
            'change input[type="file"]': 'fileChange'
        }
    }
    constructor(options) {
        super(options);
        this.data = options.data;
        this.extraData = options.extraData;
        this.file = null;
        this.render();
    }
    render() {
        this.setElement(this.template);
        this.$el.modal('show');
        this.$el.find('[autofocus]').focus();
        this.$el.find('[autofocus]').val(this.$el.find('[autofocus]').val()); // autofocus to the end of text in input
        this.$el.on('hidden.bs.modal', e => {
            this.remove();
        });
        return this;
    }
    fileChange(e) {
        this.file = e.target.files[0];
        let name = this.file.name.split('.').shift();
        this.$el.find($(e.target).data('target')).val(name);
    }
    submit() {
        let parsed = {};
        let data = this.$el.find('form').serializeArray();
        for (let { name, value } of data) {
            parsed[name] = value;
        }
        if (this.file)
            parsed['uploaded_file'] = this.file;
        this.trigger('modal:submit', parsed);
        this.file = null;
    }
    close() {
        this.$el.modal('hide');

    }
}
