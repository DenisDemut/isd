'use strict';

import './style';
import 'noty';

import { View } from 'backbone';

const template = require("ejs!./template");

export default class Component extends View {
    get template() {
        return template(this.model.toJSON());
    }
    get className() {
        return 'list-group-item';
    }
    get tagName() {
        return 'div';
    }
    get events() {
        return {
            'click .delete': 'deleteComponent',
            'click .edit': 'edit'
        }
    }
    constructor(options) {
        super(options);
        this.extraData = options.extraData;
        this.eventBus = options.events;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
        this.listenTo(this.model, 'destroy', this.unbind);
        this.listenTo(this.model, 'trigger:edit', this.editComponent);
    }
    render() {
        this.$el.html(this.template);
        return this;
    }
    deleteComponent(e) {
        e.preventDefault();
        e.stopPropagation();
        noty({
            type: 'confirm',
            text: 'Do you want to continue?',
            layout: 'topRight',
            theme: 'relax',
            buttons: [{
                addClass: 'btn btn-danger',
                text: "No",
                onClick: noty => {
                    noty.close();
                }
            }, {
                addClass: 'btn btn-success',
                text: "Yes",
                onClick: noty => {
                    this.model.destroy();
                    noty.close();
                }
            }]
        });
    }
    edit(e) {
        e.preventDefault();
        e.stopPropagation();
        this.editComponent();
    }
    editComponent() {
        let component = new this.Modal({
            gType: this.model.get('g_type'),
            data: this.model.toJSON(),
            extraData: this.extraData
        });
        component.on('modal:submit', (data) => {
            this.model.save(data, {
                wait: true,
                success: () => {
                    component.close();
                }
            });
        });
    }

}
