'use strict';
import { View, Events } from 'backbone';

export default class Components extends View {
    constructor(options) {
        super(options);
        this.types = options.types;
        this.gType = this.types.findWhere({ checked: true });
        this.extraData = options.extraData;
        this.listenToOnce(this.collection, 'sync', this.render);
        this.listenTo(this.types, 'change:checked', this.changeType);
        this.eventBus = $.extend({}, Events);
        this.views = [];

    }
    renderOne(component) {
        let c = new this.Component({
            model: component,
            extraData: this.extraData,
            events: this.eventBus
        });
        this.views.push(c);
        this.$el.append(c.render().el);
    }
    clearViews() {
        for(let view of this.views) {
            view.remove();
            view.unbind();
        }
        this.views.length = 0;
    }
    render() {
        this.clearViews();
        this.stopListening(this.collection, 'add');
        this.$el.html('');
        this.collection.filter({ 'g_type': this.gType.get('key') }).forEach(component => {
            this.renderOne(component);
        });
        this.listenTo(this.collection, 'add', this.renderOne);
    }
    addNewComponent(options) {
        // let newModel = new this.ComponentModel({
        //     g_type: this.gType.get('key'),
        // });
        // if(options) {
        //     newModel.set(options);
        // }
        let component = new this.ComponentModal({
            data: options || {},
            extraData: this.extraData
        });

        component.on('modal:submit', (data) => {
            data.g_type = this.gType.get('key');
            this.collection.create(data, {
                wait: true,
                success: () => {
                    component.close();

                }
            });
        });
    }
    changeType(data) {
        this.gType = data;
        this.render();
    }
}
