'use strict';

import { Model } from 'backbone';

export default class Region extends Model {
    get defaults() {
        return {
            'name_uk': '',
            'name_ru': '',
            'name_en': '',
            'title_uk': '',
            'title_ru': '',
            'title_en': '',
            'lat': '',
            'lng': '',
            'g_type': ''
        }
    }
    constructor(attributes, options) {
        super(attributes, options);
        this.set('name', attributes.name_ru);
        this.listenTo(this, 'change:name_ru', this.updateName);
    }
    updateName(value) {
        this.set('name', value.get('name_ru'));
    }

}
