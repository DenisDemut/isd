'use strict';

import { View } from 'backbone';

import Component from '../../components/component';
import Modal from '../modal';

export default class Region extends Component {
    constructor(options) {
        super(options);
        this.Modal = Modal;
    }
}
