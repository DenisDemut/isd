'use strict';

import Components from '../components';

import Region from './region';
import RegionModel from './region/model';
import RegionModal from './modal';

export default class Regions extends Components {
    get el() {
        return '.regions-list';
    }
    constructor(options) {
        super(options);
        this.Component = Region;
        this.ComponentModel = RegionModel;
        this.ComponentModal = RegionModal;
    }
    addNewRegion(model) {
        super.addNewComponent(model);
    }
}
