'use strict';

import { Collection } from 'backbone';
import RegionModel from './region/model';

export default class RegionCollection extends Collection {
    get model() {
        return RegionModel;
    }
    get url() {
        return '/api/v1/regions';
    }
}
