'use strict';

import { Collection } from 'backbone';
import CityModel from './city/model';

export default class CityCollection extends Collection {
    get model() {
        return CityModel;
    }
    get url() {
        return '/api/v1/cities';
    }
    constructor(models, options) {
        super(models, options);
    }
}
