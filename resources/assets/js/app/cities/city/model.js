'use strict';

import { Model } from 'backbone';
import { io as jstsIo, geom } from 'jsts';
import simplify from 'simplify-js/simplify';

export default class City extends Model {

    get defaults() {
        return {
            'name': '',
            'g_type': '',
            'polygons': []
        }
    }
    constructor(attrs, options) {
        super(attrs, options);
        this.file = attrs.uploaded_file;
        this.firstFileUpload = false;
        this.listenTo(this, 'click:download', this.triggerDownload);
    }
    parse(response, options) {
        if(response.hasOwnProperty('file') && response.file && response.file.hasOwnProperty('coordinates') && response.file.coordinates) {
            let wktReader = new jstsIo.WKTReader();
            let polygons = [];
            for(let polygon of response.file.coordinates) {
                polygons.push(wktReader.read(polygon));
            }
            response.polygons = polygons;
        }
        return super.parse(response, options);
    }

    save(attrs, options = {}) {
        attrs = attrs || {};
        if (this.file instanceof File || (attrs.hasOwnProperty('uploaded_file') && attrs.uploaded_file instanceof File)) {
            if(attrs.hasOwnProperty('uploaded_file') && (attrs.uploaded_file instanceof File)) this.file = attrs.uploaded_file;
            let formData = new FormData();
            let data = this.toJSON();
            for (let attr in data) {
                formData.append(attr, data[attr]);
            }
            formData.append('uploaded_file', this.file);
            this.file = null;
            super.save(attrs, $.extend(options, {
                data: formData,
                type: 'POST',
                processData: false,
                contentType: false,
            })).done(() => {
                if(!this.firstFileUpload) this.firstFileUpload = true;
                else this.trigger('file_uploaded', this.get('id'));
            });
        } else {
            let wktWriter = new jstsIo.WKTWriter();
            let polygons = attrs.hasOwnProperty('polygons') ? attrs.polygons : [];//this.get('polygons');
            let file = this.get('file');
            file.coordinates = [];
            for(let polygon of polygons) {
                file.coordinates.push(wktWriter.write(polygon));
            }
            this.set({'file': file}, { silent: true });
            //this.set(attrs, { silent: true });
            let data = this.toJSON();
            delete data.polygons;
            delete attrs.polygon;
            options.attrs = data;
            return super.save(attrs, options);
        }
    }
    smooth(tolerance) {
        let polygons = this.get('polygons');
        let smoothed = [];
        this.previusPolygons = polygons;
        for(let polygon of polygons) {
            let smoothCoords = simplify(polygon.getCoordinates(), (parseFloat(tolerance) || .001), true);
            if(smoothCoords.length < 4) {
                smoothed.push(polygon);
            } else {
                let gf = new geom.GeometryFactory();
                let shell = gf.createLinearRing(smoothCoords);
                smoothed.push(gf.createPolygon(shell, null));
            }
        }
        this.save({ 'polygons': smoothed }, { wait: true });
    }
    triggerDownload() {
        window.location = this.url() + '/download';
        /*return (this.sync || Backbone.sync).call(this, null, this, {
            url: this.url() + '/download',
            type: 'GET'
        });*/
    }

}
