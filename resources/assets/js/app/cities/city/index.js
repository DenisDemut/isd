'use strict';
import './style';

import { View } from 'backbone';

import Component from '../../components/component';
import Modal from '../modal';

const template = require("ejs!./template");

export default class City extends Component {
    get template() {
        return template(this.model.toJSON());
    }
    get events() {
        return $.extend(super.events, {
            'click .smooth': 'smoothCity',
            'click .download': 'downloadCity'
        });
    }
    constructor(options) {
        super(options);
        this.Modal = Modal;
    }
    smoothCity(e) {
        e.preventDefault();
        e.stopPropagation();
        this.model.trigger('click:smooth', this.model.get('id'));
    }
    downloadCity(e) {
        e.preventDefault();
        e.stopPropagation();
        this.model.trigger('click:download');
    }

}
