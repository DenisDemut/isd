'use strict';

import Modal from '../../components/modal';

const template = require("ejs!./template");

export default class CityModal extends Modal {
    get template() {
        return template(this.data);
    }

    constructor(options) {
        super(options);
    }
}
