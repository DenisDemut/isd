'use strict';

import Components from '../components';

import City from './city';
import CityModel from './city/model';
import CityModal from './modal';

export default class Cities extends Components {
    get el() {
        return '.cities-list';
    }
    constructor(options) {
        super(options);
        this.Component = City;
        this.ComponentModel = CityModel;
        this.ComponentModal = CityModal;
        this.globalEventBus = options.globalEventBus;
    }
    addNewCity() {
        super.addNewComponent();
    }
}
