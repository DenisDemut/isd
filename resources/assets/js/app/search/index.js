'use strict';
import './style';

import { View } from 'backbone';
import { maps } from 'google';

import Row from './row';
import RowModel from './model';

const template = require("ejs!./template");

export default class SearchView extends View {
    get el() {
        return '.search-bar';
    }
    get template() {
        return template();
    }
    get events() {
        return {
            'submit': 'submit',
            'input [type="text"]': 'search',
            'click .search-result': 'moveToPoint'
        }
    }
    constructor(options) {
        super(options);
        this.geocoder = new maps.Geocoder();
        this.map = options.map;
        this.render();
    }
    render() {
        this.$el.html(this.template);
        return this;
    }
    renderResults(results) {
        this.$el.find('.dropdown-menu.inner').html('');
        if(results.length > 0) {
            this.$el.find('.dropdown-menu.inner').show();
            for(let i in results) {
                let result = results[i];
                this.$el.find('.dropdown-menu.inner').append(new Row({
                    model: new RowModel(result)
                }).render().el);
            }
        } else {
            this.$el.find('.dropdown-menu.inner').hide();
        }
    }
    search(e) {
        if(this.timer) clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.geocoder.geocode({ 'address': e.target.value }, (results, status) => {
                  if (status == maps.GeocoderStatus.OK) {
                      this.renderResults(results);

                  }
              });
        }, 1000);
    }
    moveToPoint(e) {
        e.preventDefault();
        this.map.moveToPosition($(e.target).closest('.search-result').data('location'));
    }
}
