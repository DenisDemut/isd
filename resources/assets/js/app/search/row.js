'use strict';


import { View } from 'backbone';

const template = require("ejs!./row-template");

export default class RowView extends View {
    get template() {
        return template(this.model.toJSON());
    }
    get tagName() {
        return 'li';
    }
    get className() {
        return 'search-result';
    }
    get attributes() {
        return {
            'data-location': JSON.stringify(this.model.get('geometry').location)
        }
    }
    constructor(options) {
        super(options);
    }
    render() {
        this.$el.html(this.template);
        return this;
    }
}
