'use strict';

import { Model } from 'backbone';

export default class District extends Model {
    get defaults() {
        return {
            'name_uk': '',
            'name_ru': '',
            'name_en': '',
            'g_type': ''
        }
    }
    constructor(attributes, options) {
        super(attributes, options);
        this.set('name', attributes.name_ru);
        this.listenTo(this, 'change:name_ru', this.updateName);
    }
    updateName(value) {
        this.set('name', value.get('name_ru'));
    }
    parse(response, options) {
        if(response.hasOwnProperty('region')) {
            response.region = parseInt(response.region);
        }
        return super.parse(response, options);
    }
}
