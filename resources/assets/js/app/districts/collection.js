'use strict';

import { Collection } from 'backbone';
import DistrictModel from './district/model';

export default class DistrictCollection extends Collection {
    get model() {
        return DistrictModel;
    }
    get url() {
        return '/api/v1/districts';
    }
}
