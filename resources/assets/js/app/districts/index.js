'use strict';

import Components from '../components';

import District from './district';
import DistrictModel from './district/model';
import DistrictModal from './modal';

export default class Districts extends Components {
    get el() {
        return '.districts-list';
    }
    constructor(options) {
        super(options);
        this.Component = District;
        this.ComponentModel = DistrictModel;
        this.ComponentModal = DistrictModal;
    }
    addNewDistrict(model) {
        super.addNewComponent(model);
    }
}
