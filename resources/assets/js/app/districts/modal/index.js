'use strict';

import _ from 'underscore';

import Modal from '../../components/modal';
const template = require("ejs!./template");

export default class DistrictModal extends Modal {
    get template() {
        return template({
            _: _,
            data: this.data,
            regions: this.extraData.regions.toJSON(),
        });
    }

    constructor(options) {
        super(options);
    }
}
