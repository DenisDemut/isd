'use strict';

import { View } from 'backbone';

import Type from './type';

export default class TypesView extends View {
    get el() {
        return '.types-list'
    }
    constructor(options) {
        super(options);
        this.listenTo(this.collection, 'change:checked', this.updateType);
        this.render();
    }
    renderOne(type) {
        let t = new Type({
            model: type
        });
        this.$el.find('.dropdown-menu').append(t.render().el);
        if(type.get('checked')) {
            this.updateType(type);

        }
    }
    render() {
        this.$el.find('.dropdown-menu').html('');
        this.collection.each(type => {
            this.renderOne(type);
        });
        return this;
    }
    updateType(type) {
        this.$el.find('button > .current-type').text(type.get('name'));
    }
}
