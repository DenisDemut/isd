'use strict';

import { View } from 'backbone';

const template = require("ejs!./template");

export default class Type extends View {
    get events() {
        return {
            'click': 'changeType'
        }
    }
    get template() {
        return template(this.model.toJSON());
    }
    get tagName() {
        return 'li';
    }

    constructor(options) {
        super(options);
    }
    render() {
        this.$el.html(this.template);
        return this;
    }
    changeType(e) {
        e.preventDefault();
        this.model.set('checked', true);
    }
}
