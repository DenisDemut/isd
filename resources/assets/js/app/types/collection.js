'use strict';

import { Collection } from 'backbone';
import Type from './model';

export default class TypeCollection extends Collection {
    get model() {
        return Type;
    }
    get url() {
        return '/types.json';
    }
}
