'use strict';

import { maps } from 'google';

export default class PolygonView {
    constructor(options) {
        this.map = options.map;
        this.gType = options.gType;
        this.render();
    }
    remove() {
        if(this.kml) {
            this.kml.setMap(null);
            delete this.kml;
        }
    }
    render() {
        this.kml = new maps.KmlLayer(`${window.location.origin}/temp/${this.gType}/map.kml?time=${(new Date()).getTime()}`, {
            suppressInfoWindows: true,
            preserveViewport: true,
            map: this.map
        });
        console.log(this.kml, 'after render');
    }
}
