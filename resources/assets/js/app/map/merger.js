'use strict';

import { io as jstsIo, geom, operation } from 'jsts';

import MakeValid from './make-valid';

export default class Merger {
    get polygons() {
        return this._polygons;
    }
    constructor(cities) {
        this.cities = cities;
        this._polygons = new Map();
    }

    loadPolygons(cityId, list) {
        let multiPolygon = new geom.MultiPolygon(list || [], new geom.GeometryFactory());
        this._polygons.set(cityId, multiPolygon);
    }
    setupPolygons(cityId, polygons) {
        let list = [];
        let mps = [];
        for (let i = 0; i < polygons.length; i++) {
            let polygon = polygons[i];
            if(!polygon.isValid()) {
                polygon = MakeValid.make(polygon);
                if (polygon instanceof geom.MultiPolygon) {
                    mps.push(polygon);
                    continue;
                }
            }
            list.push(polygon);
        }
        this._appendMultiPolygons(list, mps, cityId);
        let multiPolygon = this._mergePolygons(list);
        this._polygons.set(cityId, multiPolygon);
        let c1 = this.cities.findWhere({id: cityId});
        c1.save({'polygons': multiPolygon.geometries});
    }
    _appendMultiPolygons(list, mps, cityId) {
        let size = list.length + 1;
        for(let i = 0; i < mps.length; i++) {
            for(let j = 0; j < mps[i].geometries.length; j++) {
                let polygon = mps[i].geometries[j];
                list.push(polygon);
            }
        }
    }
    _mergePolygons(list) {
        let mp = new geom.MultiPolygon(list, new geom.GeometryFactory());
        let nMp = mp.union();
        if (nMp instanceof geom.MultiPolygon) {
            return this._removeHoles(nMp.geometries);
        } else if (nMp instanceof geom.Polygon) {
            return this._removeHoles([nMp]);
        } else if (Object.prototype.toString.call(nMp) === '[object Array]'){
            return this._removeHoles(nMp);
        } else {
            return nMp;
        }
    }
    _removeHoles(list) {
        let polygons = [];
        for(let i = 0; i < list.length; i++) {
            let poly = list[i];
            let gf = new geom.GeometryFactory();
            let outerRing = poly.getExteriorRing().getCoordinates();
            let shell = gf.createLinearRing(outerRing);
            let polygon = gf.createPolygon(shell, null);
            polygons.push(polygon);
        }
        let mp = new geom.MultiPolygon(polygons, new geom.GeometryFactory());
        return mp;
    }

    mergeCities() {
        for(let [city1, mp1] of this._polygons) {
            for(let [city2, mp2] of this._polygons) {
                if(city1 != city2 && mp1.intersects(mp2)) {
                    if(mp1.overlaps(mp2)) {
                        let destroy = mp1.getLength() > mp2.getLength() ? city2 : city1;
                        let c = this.cities.findWhere({ id: destroy });
                        c.destroy();
                   } else {
                        let union = mp1.union(mp2);
                        this._polygons.set(city1, union);
                        let c1 = this.cities.findWhere({id: city1});
                        c1.save({'polygons': union.geometries});
                        let c2 = this.cities.findWhere({id: city2});
                        c2.destroy();
                    }
                }
            }
        }
    }
    deleteCity(cityId) {
        if(this._polygons.has(cityId)) this._polygons.delete(cityId);
    }
    clear() {
        this._polygons.clear();
    }

}
