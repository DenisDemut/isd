'use strict';

import {
    io as jstsIo,
    geom, operation
} from 'jsts';

export default class MakeValid {
    static make(object) {
        if (object instanceof geom.Polygon) {
            if (object.isValid()) {
                object.normalize();
                return object;
            }
            let polygonizer = new operation.polygonize.Polygonizer();
            this._addPolygon(object, polygonizer);
            return this._toPolygonGeometry(polygonizer.getPolygons(), object.getFactory());
        }/* else if (object instanceof jsts.object.MultiPolygon) {
            if (object.isValid()) {
                object.normalize(); // validate does not pick up rings in the wrong order - this will fix that
                return object; // If the multipolygon is valid just return it
            }
            let polygonizer = new jsts.operation.polygonize.Polygonizer();

            for (let n = object.getNumobjectetries(); n > 0; n--) {
                jsts_addPolygon(object.getobjectetryN(n - 1), polygonizer);
            }
            return jsts_toPolygonGeometry(polygonizer.getPolygons(), object.getFactory());
        } else {
            return object; // In my case, I only care about polygon / multipolygon objectetries
        }*/
    };
    static _addPolygon(polygon, polygonizer) {
        this._addLineString(polygon.getExteriorRing(), polygonizer);

        for (let n = polygon.getNumInteriorRing(); n > 0; n--) {
            this._addLineString(polygon.getInteriorRingN(n), polygonizer);
        }
    };


    static _addLineString(lineString, polygonizer) {

        if (lineString instanceof geom.LinearRing) {
            lineString = lineString.getFactory().createLineString(lineString.getCoordinateSequence());
        }
        let point = lineString.getFactory().createPoint(lineString.getCoordinateN(0));
        let toAdd = lineString.union(point);
        polygonizer.add(toAdd);
    }
    static _toPolygonGeometry(polygons, factory) {
        switch (polygons.size()) {
            case 0:
                return null;
            case 1:
                return polygons.iterator().next();
            default:
                //polygons may still overlap! Need to sym difference them
                let iter = polygons.iterator();
                let ret = iter.next();
                while (iter.hasNext()) {
                    ret = ret.symDifference(iter.next());
                }
                return ret;
        }
    }
}
