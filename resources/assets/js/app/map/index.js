'use strict';

//import _ from 'underscore';
import Backbone from 'backbone';
import { View } from 'backbone';
import { maps } from 'google';
import { io as jstsIo, geom } from 'jsts';
import MarkerClusterer from 'markerclusterer';
import simplify from 'simplify-js/simplify';
import Merger from './merger';
import PolygonView from './polygon-view';
import MarkerView from './marker-view';

const img = require('./cluster.png');

export default class MapView extends View {
    get el() {
        return '#map';
    }
    constructor(options) {
        super(options);
        this.cities = options.cities;
        this.merger = new Merger(options.cities);
        this.markers = options.markers;
        this.types = options.types;
        this.gType = this.types.findWhere({ checked: true });

        this.globalEventBus = options.globalEventBus;

        this.$el.height(options.height);
        this.map = new maps.Map(this.$el[0], {
            center: {
                lat: 50.4501,
                lng: 30.5234
            },
            zoom: 7
        });

        this.markerCluster = new MarkerClusterer(this.map, [], {
            gridSize: 50,
            maxZoom: 15,

            styles: [{
                textColor: 'white',
                url: img,
                height: 51,
                width: 51,
                textSize: 18
            }]
        });

        this.markersViews = [];

        this.listenToOnce(this.cities, 'sync', this.renderCities);
        this.listenTo(this.cities, 'sync', this.renderKMZ);

        this.listenTo(this.cities, 'file_uploaded', this.updateCityPolygons);
        this.listenTo(this.cities, 'destroy', this.deleteFromMerger);
        this.listenTo(this.cities, 'click:smooth', this.smooth);
        //this.listenTo(this.cities, 'change:polygons', this.renderKMZ);

        this.listenToOnce(this.markers, 'sync', this.renderMarkers);
        this.listenTo(this.types, 'change:checked', this.changeType);
        this.globalEventBus.on('marker:select', this.moveToMarker.bind(this))
    }
    updateCityPolygons(cityId) {
        let city = this.cities.findWhere({ id: cityId });
        this.merger.setupPolygons(city.get('id'), city.get('polygons'));
        this.merger.mergeCities();
    }

    renderCities() {
        this.stopListening(this.cities, 'add');
        this.cities.filter({
            'g_type': this.gType.get('key')
        }).forEach(city => {
            this.merger.loadPolygons(city.get('id'), city.get('polygons'));
        });

        this.listenTo(this.cities, 'add', this.loadNewCity);
    }

    loadNewCity(city) {
        this.trigger('render:start');
        setTimeout(() => {
            //this.renderCity(city);
            this.merger.setupPolygons(city.get('id'), city.get('polygons'));
            this.merger.mergeCities();
            this.trigger('render:end');
        }, 1000)
    }
    renderKMZ() {
        
        console.log('RENDER KMZ');
        if(this.kml) this.kml.remove();
        this.kml = new PolygonView({
            map: this.map,
            gType: this.gType.get('key')
        });

    }

    smooth(cityId) {
        console.log(cityId);
        let city = this.cities.findWhere({ id: cityId });

        city.smooth(this.gType.get('tolerance'));
    }
    deleteFromMerger(city) {
        this.merger.deleteCity(city.get('id'));
        this.renderKMZ();
    }




    renderMarkers() {
        this.stopListening(this.markers, 'add');
        this.markers.filter({
            'g_type': this.gType.get('key')
        }).forEach(marker => {
            this.renderMarker(marker);
        });
        this.listenTo(this.markers, 'add', this.renderMarker);
    }
    renderMarker(marker) {
        this.markersViews.push(new MarkerView({
            model: marker,
            map: this.map,
            markerCluster: this.markerCluster
        }));
    }
    clear() {
        this.kml.remove();
        for(let mv of this.markersViews) {
            mv.remove();
        }
        this.markersViews.length = 0;
        this.merger.clear();

    }

    changeType(data) {
        this.clear();
        this.gType = data;
        this.renderCities();
        this.renderKMZ();
        this.renderMarkers();
    }

    moveToPosition(position) {
        this.map.panTo(position);
        this.map.setZoom(13);
    }
    moveToMarker(id) {
        for(let i = 0; i < this.markersViews.length; i++) {
            if(this.markersViews[i].model.get('id') == id) {
                let marker = this.markersViews[i].marker;
                this.moveToPosition(marker.getPosition());
                new maps.event.trigger(marker, 'click');
                break;
            }
        }
    }

}
