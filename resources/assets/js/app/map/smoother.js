'use strict';

import { geom } from 'jsts';

class InterpPoint {
    get t() {
        return this._t;
    }
    get tsum() {
        return this._tsum;
    }
    set tsum(val) {
        this._tsum = val;
    }
    constructor() {
        this._t = [];
        this._tsum = 0;
    }
}
class SmootherControl {
    getMinLength() {
        return 0;
    }

    getNumVertices(length) {
        return 10;
    }
}

export default class Smoother {
    constructor() {
        this.control = new SmootherControl()
    }
    smooth(p, alpha) {
        let coords = p.getExteriorRing().getCoordinates();
        const N = coords.length - 1;
        let controlPoints = this.getPolygonControlPoints(coords, N, alpha);
        let smoothCoords = [];
        let dist;
        for (let i = 0; i < N; i++) {
            let next = (i + 1) % N;

            dist = coords[i].distance(coords[next]);
            if (dist < this.control.getMinLength()) {
                smoothCoords.push(coords[i]);
            } else {
                let smoothN = this.control.getNumVertices(dist);
                let segment = this.cubicBezier(
                        coords[i], coords[next],
                        controlPoints[i][1], controlPoints[next][0],
                        smoothN);

                let copyN = i < N - 1 ? segment.length - 1 : segment.length;
                for (let k = 0; k < copyN; k++) {
                    smoothCoords.push(segment[k]);
                }
            }
        }
        let gf = new geom.GeometryFactory();
        let shell = gf.createLinearRing(smoothCoords);
        return gf.createPolygon(shell, null);
    }

    getPolygonControlPoints(coords, N, alpha) {
        if (alpha < 0.0 || alpha > 1.0) {
            throw new Error("alpha must be a value between 0 and 1 inclusive");
        }
        let ctrl = new Array(N);
        for(let i = 0; i < ctrl.length; i++) {
            ctrl[i] = new Array(2);
        }
        let v = [];

        let mid = [];
        mid[0] = new geom.Coordinate();
        mid[1] = new geom.Coordinate();

        let anchor = new geom.Coordinate();
        let vdist = [];

        v[1] = coords[N - 1];
        v[2] = coords[0];
        mid[1].x = parseFloat(v[1].x + v[2].x) / 2.0;
        mid[1].y = parseFloat(v[1].y + v[2].y) / 2.0;
        vdist[1] = v[1].distance(v[2]);

        for (let i = 0; i < N; i++) {
            v[0] = v[1];
            v[1] = v[2];
            v[2] = coords[(i + 1) % N];

            mid[0].x = mid[1].x;
            mid[0].y = mid[1].y;
            mid[1].x = parseFloat(v[1].x + v[2].x) / 2.0;
            mid[1].y = parseFloat(v[1].y + v[2].y) / 2.0;

            vdist[0] = vdist[1];
            vdist[1] = v[1].distance(v[2]);

            let p = parseFloat(vdist[0]) / parseFloat(vdist[0] + vdist[1]);
            anchor.x = mid[0].x + p * (mid[1].x - mid[0].x);
            anchor.y = mid[0].y + p * (mid[1].y - mid[0].y);

            let xdelta = anchor.x - v[1].x;
            let ydelta = anchor.y - v[1].y;

            ctrl[i][0] = new geom.Coordinate(
                    alpha * (v[1].x - mid[0].x + xdelta) + mid[0].x - xdelta,
                    alpha * (v[1].y - mid[0].y + ydelta) + mid[0].y - ydelta);

            ctrl[i][1] = new geom.Coordinate(
                    alpha * (v[1].x - mid[1].x + xdelta) + mid[1].x - xdelta,
                    alpha * (v[1].y - mid[1].y + ydelta) + mid[1].y - ydelta);
        }

        return ctrl;
    }




    cubicBezier(start, end, ctrl1, ctrl2, nv) {
        let curve = new Array(nv);
        let buf = [];

        for (let i = 0; i < 3; i++) {
            buf[i] = new geom.Coordinate();
        }

        curve[0] = start;
        curve[nv - 1] = end;
        let ip = this.getInterpPoints(nv);
        for (let i = 1; i < nv-1; i++) {
            let c = new geom.Coordinate();

            c.x = ip[i].t[0] * start.x + ip[i].t[1] * ctrl1.x + ip[i].t[2] * ctrl2.x + ip[i].t[3] * end.x;
            c.x /= parseFloat(ip[i].tsum);
            c.y = ip[i].t[0] * start.y + ip[i].t[1] * ctrl1.y + ip[i].t[2] * ctrl2.y + ip[i].t[3] * end.y;
            c.y /= parseFloat(ip[i].tsum);

            curve[i] = c;
        }

        return curve;
    }
    getInterpPoints(npoints) {
        let ip = [];

        for (let i = 0; i < npoints; i++) {
            let t = parseFloat(i) / parseFloat(npoints - 1);
            let tc = 1.0 - t;

            let buf = new InterpPoint();
            buf.t.push(tc * tc * tc);
            buf.t.push(3.0 * tc * tc * t);
            buf.t.push(3.0 * tc * t * t);
            buf.t.push(t * t * t);
            buf.tsum = buf.t[0] + buf.t[1] + buf.t[2] + buf.t[3];

            ip.push(buf);
        }

        return ip;
    }
}
