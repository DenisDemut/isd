'use strict';

import { View } from 'backbone';
import { maps } from 'google';

export default class MarkerView extends View {
    constructor(options) {
        super(options);
        this.map = options.map;
        this.markerCluster = options.markerCluster;
        this.listenTo(this.model, 'destroy', this.remove);
        this.render();
    }
    clear() {
        maps.event.clearListeners(this.marker);
        this.markerCluster.removeMarker(this.marker);
        this.marker.setMap(null);
        this.marker = null;
        this.infowindow = null;
    }
    remove() {
        this.clear();
        this.unbind();
        return super.remove();
    }

    render() {
        this.marker = new maps.Marker({
            draggable: !!this.model.get('is_new'),
            position: {
                lat: parseFloat(this.model.get('lat')),
                lng: parseFloat(this.model.get('lng'))
            },
            title: this.model.get('name'),
            icon: this.model.icon,
        });
        if(!!this.model.get('is_new')) {
            this.marker.addListener('dragend', event => {
                this.model.set({
                    lat: event.latLng.lat(),
                    lng: event.latLng.lng()
                });
                this.model.save();
            });
        }
        this.infowindow = new google.maps.InfoWindow({
            content: this.model.get('name')
        });
        this.marker.addListener('click', this.bindEvents.bind(this));
        this.marker.addListener('click', () => {
            this.model.trigger('trigger:edit');
        });
        this.marker.setMap(this.map);
        this.markerCluster.addMarker(this.marker);
    }
    bindEvents() {
        this.map.panTo(this.marker.getPosition());
        this.infowindow.open(this.map, this.marker);
    }

}
