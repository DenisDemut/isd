
'use strict';

import { View } from 'backbone';

import Component from '../../components/component';

const template = require("ejs!./template");

export default class Publish extends Component {
    get template() {
        return template(this.model.toJSON());
    }
    get events() {
        return $.extend(super.events, {
            'change [name="current"]': 'changeCurrent'
        });
    }
    constructor(options) {
        super(options);
        this.Modal = null;
    }
    changeCurrent() {
        this.model.set('current', true);
        this.model.save();
    }
}
