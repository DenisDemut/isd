'use strict';

import { Collection } from 'backbone';
import PublishModel from './publish/model';
import _ from 'underscore';

export default class PublishCollection extends Collection {
    get model() {
        return PublishModel;
    }
    get url() {
        return '/api/v1/publishes';
    }
    constructor(models, options) {
        super(models, options);
        this.listenTo(this, 'change:current', this.updateCurrent);
    }
    updateCurrent(model) {
        let buf = new PublishCollection(this.without(model));
        let previus = buf.findWhere({ current: 1 });
        if(previus) {
            previus.set({'current': false}, { silent: true });
            previus.save();
        }
    }
}
