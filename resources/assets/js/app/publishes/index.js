'use strict';

import Components from '../components';

import Publish from './publish';
import PublishModel from './publish/model';

export default class Publishes extends Components {
    get el() {
        return '.publishes-list';
    }
    constructor(options) {
        super(options);
        this.Component = Publish;
        this.ComponentModel = PublishModel;
        this.ComponentModal = null;
    }

}
