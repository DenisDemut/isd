'use strict';
import "./app.css";


import 'bootstrap/js/tooltip';
import 'bootstrap/js/dropdown';
import 'bootstrap/js/button';
import 'bootstrap/js/tab';

import './admin';

import Backbone from 'backbone';
import MapView from './map';

import Cities from './cities';
import CityModal from './cities/modal';
import CityCollection from './cities/collection';

import Markers from './markers';
import MarkerModal from './markers/modal';
import MarkerCollection from './markers/collection';

import Regions from './regions';
import RegionModel from './regions/region/model';
import RegionCollection from './regions/collection';

import Districts from './districts';
import DistrictCollection from './districts/collection';

import TypesView from './types';
import TypeCollection from './types/collection';

import Search from './search';

import Publishes from './publishes';
import PublishCollection from './publishes/collection';

class AppView extends Backbone.View {
    get el() {
        return 'body';
    }
    get events() {
        return {
            'click .new-city': 'newCity',
            'click .new-marker': 'newMarker',
            'click .new-region': 'newRegion',
            'click .new-district': 'newDistrict',
            'click .publish': 'publish',
            'click .import': 'importMarkers'
        }
    }
    constructor() {
        super();
        this.types = new TypeCollection();
        this.listenToOnce(this.types, 'sync', this.render);
        this.listenTo(this.types, 'change:checked', this.changeChecked);
        this.types.fetch();
        this.globalEventBus = $.extend({}, Backbone.Events);
    }
    render() {

        new TypesView({
            collection: this.types
        });

        let cities = new CityCollection();
        let markers = new MarkerCollection();
        let regions = new RegionCollection();
        let districts = new DistrictCollection();
        this.publishesCollection = new PublishCollection();


        let height = $(document).height() - $('.main-header').outerHeight(true);
        $('.sidebar').height(height);
        $('.tab-content').height(height - $('.nav.nav-tabs').outerHeight(true));
        this.cities = new Cities({
            collection: cities,
            types: this.types,
            globalEventBus: this.globalEventBus
        });

        this.markers = new Markers({
            collection: markers,
            types: this.types,
            extraData: {
                regions: regions,
                districts: districts
            },
            globalEventBus: this.globalEventBus
        });


        this.regions = new Regions({
            collection: regions,
            types: this.types
        });

        this.districts = new Districts({
            collection: districts,
            types: this.types,
            extraData: {
                regions: regions
            }
        });
        this.map = new MapView({
            cities: cities,
            markers: markers,
            types: this.types,
            height: height - $('.menu-row').outerHeight(true),
            globalEventBus: this.globalEventBus
        });
        this.map.on('render:start', () => {
            this.$el.addClass('loading');
        });
        this.map.on('render:end', () => {
            this.$el.removeClass('loading');
        });
        this.search = new Search({
            map: this.map
        });

        this.publishes = new Publishes({
            collection: this.publishesCollection,
            types: this.types
        });

        cities.fetch();
        this.publishesCollection.fetch();
        markers.fetch();
        regions.fetch();
        districts.fetch();
    }
    newCity() {
        this.cities.addNewCity();
    }
    newMarker() {
        let center = this.map.map.getCenter();
        this.markers.addNewMarker({
            lat: center.lat(),
            lng: center.lng()
        });
    }
    newRegion() {
        this.regions.addNewRegion();
    }
    newDistrict() {
        this.districts.addNewDistrict();
    }

    publish(e) {
        $(e.target).button('loading');
        setTimeout(() => {
            //let merged = this.map.getSmoothed();
            let gType = this.types.findWhere({ checked: true });
            Backbone.$.ajax({
                url: '/api/v1/publishes',
                method: 'POST',
                data: {
                    g_type: gType.get('key'),
                    //merged: merged
                },
                success: data => {
                    noty({
                        text: data,
                        type: 'success',
                        layout: 'topRight',
                        theme: 'bootstrapTheme',
                        timeout: false,
                        closeWith: ['button']
                    });
                    $(e.target).button('reset');
                    this.publishesCollection.fetch();
                }
            })
        }, 1000)

    }
    changeChecked(data) {
        this.types.without(data).forEach(type => {
            type.set({ 'checked': false }, { silent: true });
        });
    }
    // importMarkers() {
    //     Backbone.$.getJSON('/markers.json', data => {
    //         for(let i in data) {
    //             let row = data[i];
    //             let regionTitle = row.region_title.trim();
    //             let region = this.regions.collection.findWhere({ name_ru: regionTitle });
    //             if(!region) {
    //                 this.regions.collection.create({
    //                     name_uk: regionTitle,
    //                     name_ru: regionTitle,
    //                     name_en: regionTitle,
    //                     g_type: '3g'
    //                 }, {
    //                     wait: true,
    //                     async: false
    //                 });
    //                 region = this.regions.collection.findWhere({ name_ru: regionTitle });
    //             }
    //
    //             for(let j in row.groups) {
    //                 let group = row.groups[j];
    //                 let district = null;
    //                 let groupTitle = group.title.trim();
    //                 if(j > 0) {
    //                     district = this.districts.collection.findWhere({ name_ru: groupTitle });
    //                     if(!district) {
    //                         this.districts.collection.create({
    //                             name_uk: groupTitle,
    //                             name_ru: groupTitle,
    //                             name_en: groupTitle,
    //                             g_type: '3g',
    //                             region: region.get('id')
    //                         }, {
    //                             wait: true,
    //                             async: false
    //                         });
    //                         district = this.districts.collection.findWhere({ name_ru: groupTitle });
    //                     }
    //                 }
    //                 let markers = group.items;
    //                 for(let marker in markers) {
    //                     let mkr = markers[marker];
    //                     let mkrTitle = mkr.title.trim();
    //                     let foundMarker = this.markers.collection.findWhere({ name_ru: mkrTitle, region: region.get('id'), district: district ? district.get('id') : 0 });
    //                     if(!foundMarker) {
    //                         this.markers.collection.create({
    //                             name_uk: mkrTitle,
    //                             name_ru: mkrTitle,
    //                             name_en: mkrTitle,
    //                             g_type: '3g',
    //                             lat: mkr.lat,
    //                             lng: mkr.lon,
    //                             id: mkr.id,
    //                             region: region.get('id'),
    //                             district: district ? district.get('id') : 0
    //                         });
    //                     }
    //                 }
    //             }
    //         }
    //     });
    // }
}
$(() => {

    System.import('noty').then(() => {
        Backbone.$.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
            statusCode: {
                422: xhr => {
                    for(let error in xhr.responseJSON) {
                        for(let i = 0; i < xhr.responseJSON[error].length; i++)
                            noty({
                                text: xhr.responseJSON[error][i],
                                type: 'error',
                                layout: 'topRight',
                                theme: 'bootstrapTheme',
                                timeout: 5000
                            });
                    }
                }
            }
        });


        new AppView();

    });
});
