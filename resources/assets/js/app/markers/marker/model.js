'use strict';

import { Model } from 'backbone';

export default class Marker extends Model {
    get defaults() {
        return {
            'name_uk': '',
            'name_ru': '',
            'name_en': '',
            'g_type': '',
            'region': 0,
            'district': 0,
            'is_new': false
        }
    }
    get icon() {
        return `/${this.get('g_type')}/pointer.png`;
    }
    constructor(attributes, options) {
        super(attributes, options);
        this.set('name', attributes.name_ru);
        this.listenTo(this, 'change:name_ru', this.updateName);
    }
    updateName(value) {
        this.set('name', value.get('name_ru'));
    }
    parse(response, options) {
        if(response.hasOwnProperty('region')) {
            response.region = parseInt(response.region);
        }
        if(response.hasOwnProperty('district')) {
            response.district = parseInt(response.district);
        }
        return super.parse(response, options)

    }

}
