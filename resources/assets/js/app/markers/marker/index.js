'use strict';

import './style';

import { View } from 'backbone';

import Component from '../../components/component';
import Modal from '../modal';

export default class Marker extends Component {
    get events() {
        return $.extend(super.events, {
            'click': 'selectMarker'
        });
    }
    constructor(options) {
        super(options);
        this.Modal = Modal;
    }
    selectMarker(e) {
        e.preventDefault();
        e.stopPropagation();
        this.eventBus.trigger('model:click', this.model.get('id'));
    }
}
