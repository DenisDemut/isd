'use strict';

import { Collection } from 'backbone';
import MarkerModel from './marker/model';

export default class MarkerCollection extends Collection {
    get model() {
        return MarkerModel;
    }
    get url() {
        return '/api/v1/markers';
    }
}
