'use strict';

import Modal from '../../components/modal';
import _ from 'underscore';
const template = require("ejs!./template");

export default class MarkerModal extends Modal {
    get template() {
        return template({
            _: _,
            data: this.data,
            regions: this.extraData.regions.toJSON()
        });
    }
    get events() {
        return $.extend(super.events, {
            'change select[name="region"]': 'changeRegion'
        });
    }
    constructor(options) {
        super(options);
        $('select[name="region"]').trigger('change');
    }
    changeRegion(el) {
        let target = this.$el.find('select[name="district"]');
        target.html($('<option/>').attr({ 'value': '' }).text(''));
        this.extraData.districts.where({ region: parseInt($(el.target).val()) }).forEach(district => {
            target.append($('<option/>').attr({ 'value': district.get('id'), 'selected': district.get('id') == this.model.get('district') }).text(district.get('name_ru')));
        });

    }
}
