'use strict';

import Components from '../components';

import Marker from './marker';
import MarkerModel from './marker/model';
import MarkerModal from './modal';

export default class Markers extends Components {
    get el() {
        return '.markers-list';
    }
    constructor(options) {
        super(options);
        this.Component = Marker;
        this.ComponentModel = MarkerModel;
        this.ComponentModal = MarkerModal;
        this.listenTo(this.eventBus, 'model:click', this.selectMarker);
        this.globalEventBus = options.globalEventBus;
    }
    addNewMarker(model) {
        super.addNewComponent(model);
    }
    selectMarker(id) {
        this.globalEventBus.trigger('marker:select', id);
    }
}
