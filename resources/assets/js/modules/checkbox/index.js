import 'icheck/skins/square/blue.css';
System.import('icheck/icheck').then(() => {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%',
        handle: 'checkbox'
    });
});
