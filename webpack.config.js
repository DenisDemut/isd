'use strict';
const webpack = require('webpack');
const path = require('path');
const ENV = process.env.NODE_ENV || 'development';

let config = {

    context: __dirname + '/resources/assets/js',
    entry: {
        app: './app/',
        auth: './auth/'
    },
    output: {
        path: __dirname + '/public/js',
        filename: '[name].js',
        publicPath: '/js/'
    },
    module: {
        loaders: [{
            test: /\.css$/,
            loaders: ['style', 'css']

        }, {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)\/(?!jsts)/,
            loader: 'babel',
            query: {
                presets: ['es2015']
            }
        }, {
            test: /\.(svg|ttf|eot|woff|woff2|png)$/,
            exclude: /resources\/assets\/js/,
            loader: 'file?name=../fonts/[name].[ext]'
        }, {
            test: /\.(jpg|jpeg|gif|png)$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'url?limit=10000&name=../images/[name].[ext]'
        }, {
            test: path.resolve(__dirname, "bower_components/js-marker-clusterer/src/markerclusterer.js"),
            loader: 'exports?MarkerClusterer'
        }, {
            test: path.resolve(__dirname, "bower_components/simplify-js/simplify.js"),
            loader: 'imports?define=>false'
        }, {
            test: path.resolve(__dirname, "bower_components/Wicket/wicket-gmap3.js"),
            loader: 'imports?Wkt=>false'
        }]
    },
    resolve: {
        extensions: ['', '.js', '.css', '.html'],
        modules: [path.resolve(__dirname, "node_modules"), path.resolve(__dirname, "bower_components")],
        alias: {
            jquery: 'jquery/dist/jquery',
            icheck: 'iCheck',
            noty: 'noty/js/noty/packaged/jquery.noty.packaged',
            backbone: 'backbone/backbone',
            underscore: 'underscore/underscore',
            markerclusterer: 'js-marker-clusterer/src/markerclusterer',
            jsts: 'jsts/src/jsts.js',
            wicket: 'Wicket'
        },
        descriptionFiles: ["package.json", "bower.json"]
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": 'jquery'
        })
    ],
    externals: {
        google: 'google'
    },


    devtool: (ENV == 'production') ? null : '#cheap-module-source-map'
};
if (ENV == 'production') {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            compress: {
                warnings: false,
                unsafe: true,
                drop_console: true
            }
        })
    );
}
module.exports = config;
